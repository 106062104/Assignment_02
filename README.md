# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|N|
|Basic rules|20%|N|
|Jucify mechanisms|15%|N|
|Animations|10%|N|
|Particle Systems|10%|N|
|UI|5%|N|
|Sound effects|5%|N|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : <br>
    分成五個level：<br>
    level 1: 敵機生成慢，敵機速度慢，敵機子彈速度慢，子彈直線<br>
    level 2: 敵機生成慢，敵機速度中，敵機子彈速度中，子彈直線<br>
    level 3: 敵機生成中，敵機速度中，敵機子彈速度中，子彈不規則<br>
    level 4: 敵機生成快，敵機速度快，敵機子彈速度快，子彈不規則<br>

2. Animations : <br>
    我方： left, right<br>
    敵機： 尾焰<br>

3. Particle Systems : <br>
    我方子彈：直線<br>
    敵方子彈：隨Level變更<br>
    Aiming: 朝敵機方向，命中率一般<br>

4. Sound effects : <br>
    遊戲BGM：可控音量 <br>
    我機發射子彈<br>

5. Leaderboard : <br>
    無

# Bonus Functions Description : 
1. 掉落物 :<br>
    1. itemLove：拾獲可增加一血，機率為0.1%
    2. itemBullet：拾獲可增加發射自動瞄準的子彈，0.02%

2. UI : <br>
    1. score: 左上角，一台敵機10分
    2. ultimate skill: 一場可使用一次，Enter鍵，可清場
    3. player health: 右下角
    4. volume control: 使用數字鍵盤的"+","-"
    5. pause: button，使用滑鼠點擊，點擊後會變換frame
3. Boss:<br>
    1. 攻擊模式：追蹤子彈，子彈速度中，子彈發射快
    2. 血量：1000滴血
    3. 移動模式：以不變應萬變
4. Setting:<br>
    1. setting中，有按鍵顯示，好玩！
    2. 往前方邊界移動，可直接進入遊戲
    3. 往其他邊界移動，可退回主選單
