var bootState = {
    preload: function(){
        game.load.image('progress','assets/progress.png');
    },

    create: function(){
        game.stage.backgroundColor = 'black';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        game.state.start('load');
    }
};