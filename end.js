var endState = {
    preload: function () {
    },

    create: function () {
        this.background = game.add.tileSprite(0, 0, 800, 600, 'background');

        if (win) {
            this.win = game.add.sprite(200, 50, 'win');
            this.win.scale.setTo(1, 1);
        }
        else{
            this.lose = game.add.sprite(160, 50, 'lose');
            this.lose.scale.setTo(1, 1);
        }

        this.menu = game.add.button(400,325,'menuButton',this.menuOnClick,this,1,2,0);
        this.menu.anchor.setTo(0.5,0.5);

        this.again = game.add.button(400,400,'againButton',this.againOnClick,this,1,2,0);
        this.again.anchor.setTo(0.5,0.5);
    },
    menuOnClick:function(){
        game.state.start("menu");
    },

    againOnClick:function(){
        game.state.start("play");
    },

    update: function () {
        this.background.tilePosition.y += 2;

    }
};