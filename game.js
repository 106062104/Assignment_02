
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');

game.state.add('menu',menuState);
game.state.add('end',endState);
game.state.add('play',playState);
game.state.add('boot',bootState);
game.state.add('load',loadState);
game.state.add('setting',settingState);

game.global = {
    win : true
}
game.state.start('boot');