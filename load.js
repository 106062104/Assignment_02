var loadState = {
    preload: function(){
        var loadingLabel = game.add.text(game.width/2, 150, 'loading...',{font:'30px Arial', fill:'#ffffff'});
        loadingLabel.anchor.setTo(0.5,0.5);

        var progress = game.add.sprite(game.width/2, 200, 'progress');
        progress.anchor.setTo(0.5,0.5);
        game.load.setPreloadSprite(progress);


        //menu
        game.load.image('background','assets/starfield.png');
        game.load.spritesheet('settingButton','assets/settingButton.png',227,60);
        game.load.spritesheet('startButton','assets/startButton.png',227 ,60);
        game.load.image('raiden','assets/raiden.png');

        //setting
        game.load.spritesheet('upButton','assets/upButton.png',67 ,60);
        game.load.spritesheet('downButton','assets/downButton.png',67 ,60);
        game.load.spritesheet('leftButton','assets/leftButton.png',67 ,60);
        game.load.spritesheet('rightButton','assets/rightButton.png',67 ,60);
        game.load.spritesheet('spacebarButton','assets/spacebarButton.png',227 ,60);


        //play
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        game.load.image('bossBullet', 'assets/boss-bullet.png');
        game.load.spritesheet('enemy', 'assets/enemy.png', 25, 32);
        game.load.spritesheet('flight', 'assets/flight.png',55,34)
        game.load.spritesheet('explosion', 'assets/explode.png', 128, 128);
        game.load.image('boss','assets/boss.png');
        game.load.audio('bossBgm','assets/boss.ogg');
        game.load.spritesheet('sound','assets/sound.png',38,30);
        game.load.spritesheet('pause','assets/state.png',30,30);
        game.load.audio('shoot','assets/shoot.ogg');
        game.load.image('love','assets/love.png');
        game.load.image('upgrade','assets/upgrade.png');
        game.load.image('bulletAim','assets/bulletAim.png');


        //end
        game.load.image('lose','assets/lose.png');
        game.load.image('win','assets/win.png');
        game.load.spritesheet('menuButton','assets/menuButton.png',227,60);
        game.load.spritesheet('againButton','assets/againButton.png',227 ,60);
        
        
    },

    create:function(){
        game.state.start('menu');
    }
}