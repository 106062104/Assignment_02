var menuState ={
    preload: function(){
    },

    create:function(){
        this.background = game.add.tileSprite(0, 0, 800, 600, 'background');
        
        this.title = game.add.sprite(50,50,'raiden');
        this.title.scale.setTo(1, 1);

        this.start = game.add.button(400,325,'startButton',this.startOnClick,this,1,2,0);
        this.start.anchor.setTo(0.5,0.5);

        this.setting = game.add.button(400,400,'settingButton',this.settingOnClick,this,1,2,0);
        this.setting.anchor.setTo(0.5,0.5);


    },

    startOnClick:function() {

        game.state.start("play");
    
    },

    settingOnClick:function() {
        game.state.start("setting");
    },

    update: function(){
        this.background.tilePosition.y += 2;
    }
}