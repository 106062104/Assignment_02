var playState = {
    preload: function () {

    },

    create: function () {
        this.background = game.add.tileSprite(0, 0, 800, 600, 'background');



        this.player = game.add.sprite(400, 500, 'flight');
        this.player.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;

        this.player.animations.add('left', [1], 16, true);
        this.player.animations.add('right', [4], 16, true);


        this.boss = game.add.sprite(400, -300, 'boss');
        this.boss.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.boss);

        this.bossBulletTime = 0;
        this.bossBullets = game.add.group();
        this.bossBullets.enableBody = true;
        this.bossBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bossBullets.createMultiple(100, 'bossBullet');
        this.bossBullets.setAll('anchor.x', 0.5);
        this.bossBullets.setAll('anchor.y', 1);
        this.bossBullets.setAll('outOfBoundsKill', true);
        this.bossBullets.setAll('checkWorldBounds', true);
        this.bossLive = 100;

        this.cursors = game.input.keyboard.createCursorKeys();
        this.fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);


        this.bulletTime = 0;
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(30, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 1);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);

        this.enemys = game.add.group();
        this.enemys.enableBody = true;
        this.enemys.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemys.createMultiple(30, 'enemy');
        this.enemys.setAll('anchor.x', 0.5);
        this.enemys.setAll('anchor.y', 0.5);
        this.enemys.setAll('outOfBoundsKill', true);
        this.enemys.setAll('checkWorldBounds', true);
        this.enemys.forEach(function (enemy) {
            enemy.animations.add('fly', [0, 1, 2, 3], 16, true);
        })
        this.nextEnemy = 0;

        this.enemyBulletTime = 0;
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyBullets.createMultiple(100, 'enemyBullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 1);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);

        this.score = 0;
        this.scoreString = 'Score: ';
        this.scoreTxt = game.add.text(10, 40, this.scoreString + this.score, { font: '24px Arial', fill: '#ffffff' });

        this.level = 1;
        this.levelString = "Stage: ";
        this.levelTxt = game.add.text(10, 10, this.levelString + this.level, { font: '24px Arial', fill: '#ffffff' });

        this.playerLive = 3;

        this.itemLive = game.add.sprite(685, 560, 'flight');
        this.itemLive.alpha = 0.6;
        this.liveString = "x ";
        this.liveTxt = game.add.text(750, 560, this.liveString + this.playerLive, { font: '24px Arial', fill: '#ffffff' });
        this.liveTxt.alpha = 0.6;

        /*for (var i = 0; i < 3; i++) {
            var flight = this.playerLive.create(650 + 60 * i, 580, 'flight');
            flight.anchor.setTo(0.5, 0.5);
            flight.alpha = 0.6;

        }*/

        this.music = game.add.sound('bossBgm', 0.5, true);
        this.music.play();

        this.sound = game.add.sprite(714, 10, 'sound');
        this.loudButton = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_ADD);
        this.quiteButton = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_SUBTRACT);
        this.muteButton = game.input.keyboard.addKey(Phaser.Keyboard.M);
        this.soundTime = 0;

        this.skillButton = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.skill = true;
        this.skillNum = 1;
        this.skillString = "Skill: ";
        this.skillTxt = game.add.text(10, 570, this.skillString + this.skillNum, { font: '24px Arial', fill: '#ffffff' });

        this.shoot = game.add.audio('shoot');
        this.shoot.volume = 0.2;


        this.pause = game.add.button(760, 10, 'pause', this.pauseOnClick, this, 0, 0, 0);

        this.itemLove = game.add.group();
        this.itemLove.enableBody = true;
        this.itemLove.physicsBodyType = Phaser.Physics.ARCADE;
        this.itemLove.createMultiple(10, 'love');
        this.itemLove.setAll('anchor.x', 0.5);
        this.itemLove.setAll('anchor.y', 1);
        this.itemLove.setAll('outOfBoundsKill', true);
        this.itemLove.setAll('checkWorldBounds', true);

        this.itemBullet = game.add.group();
        this.itemBullet.enableBody = true;
        this.itemBullet.physicsBodyType = Phaser.Physics.ARCADE;
        this.itemBullet.createMultiple(10, 'upgrade');
        this.itemBullet.setAll('anchor.x', 0.5);
        this.itemBullet.setAll('anchor.y', 1);
        this.itemBullet.setAll('outOfBoundsKill', true);
        this.itemBullet.setAll('checkWorldBounds', true);

        this.aiming = 0;
        this.aimBulletTime = 0;
        this.aimBullets = game.add.group();
        this.aimBullets.enableBody = true;
        this.aimBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.aimBullets.createMultiple(30, 'bulletAim');
        this.aimBullets.setAll('anchor.x', 0.5);
        this.aimBullets.setAll('anchor.y', 1);
        this.aimBullets.setAll('outOfBoundsKill', true);
        this.aimBullets.setAll('checkWorldBounds', true);

    },

    pauseOnClick: function () {

        if (this.pause.frame == 1) {
            this.pause.setFrames(0, 0, 0);
        }
        else {
            this.pause.setFrames(1, 1, 1);
        }
        game.paused = !game.paused;
    },

    update: function () {
        this.background.tilePosition.y += 2;
        //console.log(this.player.alive);

        if (this.player.alive) {

            if (this.soundTime < game.time.now) {
                if (this.muteButton.isDown) {
                    this.music.mute = !this.music.mute;
                    this.soundTime = game.time.now + 200;
                }

                if (!this.music.mute) {
                    if (this.loudButton.isDown) {

                        if (this.music.volume < 0.9) {
                            this.music.volume += 0.1;
                            this.soundTime = game.time.now + 200;
                        }
                    }
                    else if (this.quiteButton.isDown) {
                        if (this.music.volume > 0.2) {
                            this.music.volume -= 0.1;
                            this.soundTime = game.time.now + 200;
                        }
                    }
                }
            }
            console.log(this.music.volume);

            if (this.music.mute) {
                this.sound.frame = 3;
            }
            else {
                if (this.music.volume >= 0.7) {
                    this.sound.frame = 1;
                }
                else if (this.music.volume < 0.7 && this.music.volume >= 0.35) {
                    this.sound.frame = 0;
                }
                else if (this.music.volume < 0.35 && this.music.volume > 0) {
                    this.sound.frame = 2;
                }
            }



            if (this.nextEnemy < game.time.now) {
                var enemy = this.enemys.getFirstExists(false);

                if (this.level == 1) {
                    enemy.reset(game.rnd.integerInRange(20, 780), 0);
                    enemy.body.velocity.y = 50;
                    enemy.play('fly');

                    this.nextEnemy = game.time.now + 1500;
                }
                else if (this.level == 2) {
                    enemy.reset(game.rnd.integerInRange(20, 780), 0);
                    enemy.body.velocity.y = 75;
                    enemy.play('fly');

                    this.nextEnemy = game.time.now + 1500;
                }
                else if (this.level == 3) {
                    enemy.reset(game.rnd.integerInRange(20, 780), 0);
                    enemy.body.velocity.y = 75;
                    enemy.play('fly');

                    this.nextEnemy = game.time.now + 1000;
                }
                else if (this.level == 4) {
                    enemy.reset(game.rnd.integerInRange(20, 780), 0);
                    enemy.body.velocity.y = 100;
                    enemy.play('fly');

                    this.nextEnemy = game.time.now + 800;
                }
                else if (this.level == 5) {
                    this.enemys.forEachAlive(function (enemy) {
                        enemy.body.velocity.y = 300;
                        enemy.play('fly');
                    })
                    if (this.boss.y < 150) {
                        this.boss.body.velocity.y = 100;
                        //console.log(this.boss.y);
                    }
                    else {
                        this.boss.body.velocity.y = 0;
                    }
                }

            }

            if (game.time.now > this.enemyBulletTime) {
                this.enemys.forEachAlive(function (enemy) {
                    var bullet = this.enemyBullets.getFirstExists(false);
                    //console.log(bullet);
                    if (bullet) {

                        if (this.level == 1) {
                            bullet.reset(enemy.x, enemy.y + 10);
                            bullet.body.velocity.y = 100;
                            this.enemyBulletTime = game.time.now + 4000;
                            //console.log(this.enemyBulletTime);
                        }
                        else if (this.level == 2) {
                            bullet.reset(enemy.x, enemy.y + 10);
                            bullet.body.velocity.y = 200;
                            this.enemyBulletTime = game.time.now + 2000;
                            //console.log(this.enemyBulletTime);
                        }
                        else if (this.level == 3) {
                            bullet.reset(enemy.x, enemy.y + 10);
                            bullet.body.velocity.x = this.player.x - enemy.x;
                            bullet.body.velocity.y = 800 - bullet.body.velocity.x ;


                            this.enemyBulletTime = game.time.now + 2000;
                            //console.log(this.enemyBulletTime);
                        }
                        else if (this.level == 4) {
                            bullet.reset(enemy.x, enemy.y + 10);
                            bullet.body.velocity.x = this.player.x - enemy.x;
                            bullet.body.velocity.y = this.player.y - enemy.y;

                            this.enemyBulletTime = game.time.now + 2000;
                            //console.log(this.enemyBulletTime);
                        }
                        else if (this.level == 5) {

                        }

                    }
                }, this)
            }

            if (this.level == 5) {
                this.enemyBullets.forEachAlive(function (bullet) {
                    bullet.kill();
                })
                if (this.boss.alive) {
                    this.bossFire();
                }
                else {
                    this.bossBullets.forEachAlive(function (bullet) {
                        bullet.kill();
                    })

                }
            }




            if (this.boss.alive) {
                this.player.body.velocity.setTo(0, 0);
                if (this.cursors.left.isDown) {
                    this.player.body.velocity.x = -200;

                    this.player.animations.play('left');
                }
                else if (this.cursors.right.isDown) {
                    this.player.body.velocity.x = 200;

                    this.player.animations.play('right');
                }
                else {
                    this.player.frame = 0;
                }

                if (this.cursors.up.isDown) {
                    this.player.body.velocity.y = -200;
                }
                else if (this.cursors.down.isDown) {
                    this.player.body.velocity.y = 200;
                }

                if (this.fireButton.isDown) {

                    if (game.time.now > this.bulletTime) {
                        this.playerFire();
                        this.shoot.play();
                    }


                }

                if (this.skillButton.isDown) {
                    if (this.skill) {
                        this.ultimate();
                        this.skill = false;
                        this.skillNum = 0;
                        this.skillTxt.text = this.skillString + this.skillNum;
                    }
                }
            }
            else {
                this.player.kill();
                win = true;
                this.music.mute = true;
                game.state.start("end");



            }

            if (this.aiming == 1) {
                this.playerBulletAim();
            }



            game.physics.arcade.overlap(this.bullets, this.enemys, this.enemyHit, null, this);
            game.physics.arcade.overlap(this.enemyBullets, this.player, this.playerHitEnemy, null, this);
            game.physics.arcade.overlap(this.enemys, this.player, this.collison, null, this);
            game.physics.arcade.overlap(this.bullets, this.boss, this.bossHit, null, this);

            game.physics.arcade.overlap(this.bossBullets, this.player, this.playerHitBoss, null, this);
            game.physics.arcade.overlap(this.itemLove, this.player, this.playerLove, null, this);
            game.physics.arcade.overlap(this.itemBullet, this.player, this.playerBullet, null, this);

            game.physics.arcade.overlap(this.aimBullets, this.boss, this.bossHit, null, this);
            game.physics.arcade.overlap(this.aimBullets, this.enemys, this.enemyHit, null, this);



        }
        else {
            win = false;
            this.music.mute = true;
            game.state.start("end");
        }
    },

    playerBulletAim: function () {
        var bullet = this.aimBullets.getFirstExists(false);
        var enemy = this.enemys.getRandomExists();
        if (this.aimBulletTime < game.time.now) {
            if (bullet) {
                if (enemy) {
                    bullet.reset(this.player.x, this.player.y + 10);
                    game.physics.arcade.moveToObject(bullet, enemy, 500, 1500);
                    this.aimBulletTime = game.time.now + 2500;
                }
            }
        }

    },

    playerBullet: function (player, item) {
        item.kill();
        this.aiming = 1;
    },

    playerLove: function (player, item) {
        item.kill();


        if (this.playerLive == 2) {
            this.playerLive = 3;
            this.liveTxt.text = this.liveString + this.playerLive;
        }
        else if (this.playerLive == 1) {
            this.playerLive = 2;
            this.liveTxt.text = this.liveString + this.playerLive;
        }
    },

    ultimate: function () {
        this.enemyBullets.forEachAlive(function (bullet) {
            bullet.kill();
        })

        this.enemys.forEachAlive(function (enemy) {
            enemy.kill();

            var explosion = game.add.sprite(enemy.x, enemy.y, 'explosion');
            explosion.anchor.setTo(0.5);
            explosion.animations.add('boom');
            explosion.play('boom', 15, false, true);

            this.score += 10;
            //console.log(this.score);
            this.scoreTxt.text = this.scoreString + this.score;

            if (this.score == 100) {
                this.level = 2;
                this.levelTxt.text = this.levelString + this.level;
            }
            else if (this.score == 200) {
                this.level = 3;
                this.levelTxt.text = this.levelString + this.level;
            }
            else if (this.score == 400) {
                this.level = 4;
                this.levelTxt.text = this.levelString + this.level;
            }
            else if (this.score == 600) {
                this.level = 5;
                this.levelTxt.text = this.levelString + this.level;
            }
        }, this)
    },

    playerHitBoss: function (player, bullet) {
        bullet.kill();

        /*var live = this.playerLive.getFirstAlive();
        if (live) {
            live.kill();
        }*/
        this.playerLive -= 1;
        this.liveTxt.text = this.liveString + this.playerLive;

        var explosion = game.add.sprite(player.x, player.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        if (this.playerLive < 1) {
            player.kill();
        }
        else {

            player.x = 400;
            player.y = 500;
        }
    },

    bossHit: function (boss, bullet) {
        bullet.kill();

        this.bossLive -= 1;
        console.log(this.bossLive);
        var explosion = game.add.sprite(bullet.x, bullet.y, 'explosion');
        explosion.anchor.setTo(0.5, 1.2);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        if (this.bossLive == 0) {
            boss.kill();
        }

    },

    collison: function (player, enemy) {
        enemy.kill();

        /*var live = this.playerLive.getFirstAlive();
        if (live) {
            live.kill();
        }*/
        this.playerLive -= 1;
        this.liveTxt.text = this.liveString + this.playerLive;

        var explosion = game.add.sprite(player.x, player.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        this.score += 10;
        //console.log(this.score);
        this.scoreTxt.text = this.scoreString + this.score;

        if (this.score == 100) {
            this.level = 2;
            this.levelTxt.text = this.levelString + this.level;
        }
        else if (this.score == 200) {
            this.level = 3;
            this.levelTxt.text = this.levelString + this.level;
        }
        else if (this.score == 400) {
            this.level = 4;
            this.levelTxt.text = this.levelString + this.level;
        }
        else if (this.score == 600) {
            this.level = 5;
            this.levelTxt.text = this.levelString + this.level;
        }

        if (this.playerLive < 1) {
            player.kill();
        }
        else {
            player.x = 400;
            player.y = 500;
        }


    },

    enemyHit: function (enemy, bullet) {
        enemy.kill();
        bullet.kill();

        var explosion = game.add.sprite(enemy.x, enemy.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        var num = game.rnd.integerInRange(0, 1000);
        var probLove = num % 10;
        var probAim = num % 50;

        if (probLove == 0) {
            var love = this.itemLove.getFirstExists(false);

            if (love) {
                love.reset(enemy.x, enemy.y + 10);
                love.body.velocity.y = 150;
            }
        }

        if (probAim == 0) {
            var aim = this.itemBullet.getFirstExists(false);

            if (aim) {
                aim.reset(enemy.x, enemy.y + 10);
                aim.body.velocity.y = 150;
            }
        }


        this.score += 10;
        //console.log(this.score);
        this.scoreTxt.text = this.scoreString + this.score;

        if (this.score == 100) {
            this.level = 2;
            this.levelTxt.text = this.levelString + this.level;
        }
        else if (this.score == 200) {
            this.level = 3;
            this.levelTxt.text = this.levelString + this.level;
        }
        else if (this.score == 400) {
            this.level = 4;
            this.levelTxt.text = this.levelString + this.level;
        }
        else if (this.score == 600) {
            this.level = 5;
            this.levelTxt.text = this.levelString + this.level;
        }
    },




    playerHitEnemy: function (player, bullet) {
        bullet.kill();

        /*var live = this.playerLive.getTop();
        if (live) {
            live.kill();
        }*/

        this.playerLive -= 1;
        this.liveTxt.text = this.liveString + this.playerLive;

        var explosion = game.add.sprite(player.x, player.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        if (this.playerLive < 1) {
            player.kill();
        }
        else {
            player.x = 400;
            player.y = 500;
        }
    },

    playerFire: function () {


        var bullet = this.bullets.getFirstExists(false);

        if (bullet) {
            bullet.reset(this.player.x, this.player.y - 10);
            bullet.body.velocity.y = -400;
            this.bulletTime = game.time.now + 200;
        }


    },

    bossFire: function () {

        if (game.time.now > this.bossBulletTime) {
            var bullet = this.bossBullets.getFirstExists(false);

            if (bullet) {
                bullet.reset(this.boss.x, this.boss.y + 10);
                game.physics.arcade.moveToObject(bullet, this.player, 200);

                this.bossBulletTime = game.time.now + 400;
            }
        }

    },










}