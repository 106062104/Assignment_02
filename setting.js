var settingState = {
    preload: function () {
    },

    create: function () {
        this.background = game.add.tileSprite(0, 0, 800, 600, 'background');

        this.up = game.add.sprite(646, 450, 'upButton');
        this.up.frame = 1;

        this.down = game.add.sprite(646, 520, 'downButton');
        this.down.frame = 1;

        this.left = game.add.sprite(579, 520, 'leftButton');
        this.left.frame = 1;

        this.right = game.add.sprite(713, 520, 'rightButton');
        this.right.frame = 1;

        this.space = game.add.sprite(10, 520, 'spacebarButton');
        this.space.frame = 2;

        this.player = game.add.sprite(400, 500, 'flight');
        this.player.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.player);
        this.player.outOfBoundsKill = true;
        this.player.checkWorldBounds = true;

        this.player.animations.add('left', [1], 16, true);
        this.player.animations.add('right', [4], 16, true);

        this.bulletTime = 0;
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(30, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 1);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);

        this.cursors = game.input.keyboard.createCursorKeys();
        this.fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    },


    update: function () {
        this.background.tilePosition.y += 2;

        if (this.player.alive) {
            this.player.body.velocity.setTo(0, 0);
            if (this.cursors.left.isDown) {
                this.player.body.velocity.x = -200;

                this.player.animations.play('left');
                this.left.frame = 0;
            }
            else if (this.cursors.right.isDown) {
                this.player.body.velocity.x = 200;

                this.player.animations.play('right');
                this.right.frame = 0;
            }
            else {
                this.player.frame = 0;
                this.left.frame = 1;
                this.right.frame = 1;

            }

            if (this.cursors.up.isDown) {
                this.player.body.velocity.y = -200;
                this.up.frame = 0;
            }
            else if (this.cursors.down.isDown) {
                this.player.body.velocity.y = 200;
                this.down.frame = 0;
            }
            else {
                this.up.frame = 1;
                this.down.frame = 1;

            }

            if (this.fireButton.isDown) {
                this.space.frame = 0;
                this.playerBullet();
            }
            else {
                this.space.frame = 2;

            }
        }
        else {
            if (this.player.y < 0) {
                game.state.start("play");

            }
            else{
                game.state.start("menu");
            }
        }

    },

    playerBullet: function () {

        if (game.time.now > this.bulletTime) {
            var bullet = this.bullets.getFirstExists(false);

            if (bullet) {
                bullet.reset(this.player.x, this.player.y - 10);
                bullet.body.velocity.y = -400;
                this.bulletTime = game.time.now + 200;
            }
        }

    },
};